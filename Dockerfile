FROM docker.io/golang:1.12 AS builder

# S3 Bucket Exporter image for OpenShift Origin

RUN apt-get update \
    && apt-get install -y bzr \
    && rm -rf /var/lib/apt/lists/* \
    && mkdir /build

COPY config/*.go /build/
COPY config/controllers /build/controllers
WORKDIR /build

RUN go get github.com/aws/aws-sdk-go \
    && go get github.com/prometheus/client_golang/prometheus \
    && go get github.com/Sirupsen/logrus \
    && go build ./main.go \
    && cp ./main /s3bucket_exporter

FROM docker.io/debian:buster-slim

LABEL io.k8s.description="S3 Bucket Prometheus Exporter Image." \
      io.k8s.display-name="S3 Bucket Prometheus Exporter" \
      io.openshift.expose-services="9113:http" \
      io.openshift.tags="prometheus,exporter,s3" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-s3bucketexporter" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.0.0"

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive

COPY --from=builder /s3bucket_exporter /s3bucket_exporter

RUN set -x \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	rm -rf /var/lib/apt/lists/*; \
	apt-get update; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
	apt-get autoremove -y --purge; \
	apt-get clean; \
    fi \
    && rm -rvf /usr/share/man /usr/share/doc /var/lib/apt/lists/*

ENTRYPOINT ["/s3bucket_exporter"]
USER 1001
WORKDIR /tmp
